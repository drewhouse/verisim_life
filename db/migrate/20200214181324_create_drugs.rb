class CreateDrugs < ActiveRecord::Migration[6.0]
  def change
    create_table :drugs do |t|
      t.string :name
      t.string :iupac_name
      t.string :canonical_smiles
      t.string :molecular_formula
      t.string :cas_number
      t.string :molecular_weight
      t.string :klass
      t.string :superklass
      t.string :rotatable_bond_count
      t.string :complexity

      t.timestamps
    end
  end
end
