require 'test_helper'

class DrugsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @drug = drugs(:one)
  end

  test "should get index" do
    get drugs_url
    assert_response :success
  end

  test "should get new" do
    get new_drug_url
    assert_response :success
  end

  test "should create drug" do
    assert_difference('Drug.count') do
      post drugs_url, params: { drug: { canonical_smiles: @drug.canonical_smiles, cas_number: @drug.cas_number, complexity: @drug.complexity, iupac_name: @drug.iupac_name, klass: @drug.klass, molecular_formula: @drug.molecular_formula, molecular_weight: @drug.molecular_weight, name: @drug.name, rotatable_bond_count: @drug.rotatable_bond_count, superklass: @drug.superklass } }
    end

    assert_redirected_to drug_url(Drug.last)
  end

  test "should show drug" do
    get drug_url(@drug)
    assert_response :success
  end

  test "should get edit" do
    get edit_drug_url(@drug)
    assert_response :success
  end

  test "should update drug" do
    patch drug_url(@drug), params: { drug: { canonical_smiles: @drug.canonical_smiles, cas_number: @drug.cas_number, complexity: @drug.complexity, iupac_name: @drug.iupac_name, klass: @drug.klass, molecular_formula: @drug.molecular_formula, molecular_weight: @drug.molecular_weight, name: @drug.name, rotatable_bond_count: @drug.rotatable_bond_count, superklass: @drug.superklass } }
    assert_redirected_to drug_url(@drug)
  end

  test "should destroy drug" do
    assert_difference('Drug.count', -1) do
      delete drug_url(@drug)
    end

    assert_redirected_to drugs_url
  end
end
