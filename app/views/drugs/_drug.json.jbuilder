json.extract! drug, :id, :name, :iupac_name, :canonical_smiles, :molecular_formula, :cas_number, :molecular_weight, :klass, :superklass, :rotatable_bond_count, :complexity, :created_at, :updated_at
json.url drug_url(drug, format: :json)
